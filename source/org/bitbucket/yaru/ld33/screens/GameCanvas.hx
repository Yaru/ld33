package org.bitbucket.yaru.ld33.screens;

import flixel.FlxG;
import flixel.FlxState;
import openfl.system.System;

/**
 * ...
 * @author Yaru
 */
class GameCanvas extends FlxState
{

	override public function create():Void 
	{
		super.create();
		FlxG.sound.muteKeys = FlxG.sound.volumeDownKeys = FlxG.sound.volumeUpKeys = null;
	}
	
	override public function update():Void 
	{
		#if debug
		if (FlxG.keys.justPressed.ESCAPE) {
			System.exit(0);
		}
		if (FlxG.keys.justPressed.F11) {
			FlxG.resetState();
		}
		if (FlxG.keys.justPressed.F12) {
			FlxG.resetGame();
		}
		#end
		super.update();
	}
	
}